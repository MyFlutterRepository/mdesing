/// Comportamento dos componentes
enum Behaviour {
  regular,
  loading,
  error,
  empty,
  disabled,
}
