import 'package:flutter/material.dart';
import '../behaviour/behaviour.dart';
import 'component_style.dart';

abstract class BaseComponent<T, U> {
  Widget whenRegular(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  );

  Widget whenLoading(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  );

  Widget whenError(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  );

  Widget whenDisabled(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  );

  Widget whenEmpty(
    T style,
    U sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  );

  Widget render({
    required BuildContext context,
    required Behaviour behaviour,
    required ComponentStyle<T, U> componentStyle,
  });

  Map<Behaviour, Behaviour>? get delegate;
}
