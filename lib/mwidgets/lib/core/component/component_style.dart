/// ## Define o estilo do componente.
///
/// Todo componente deve ser tipado com duas classes de estilo:
/// 1. __NomeStyle__ para estilos do próprio componente.
///
/// 2. __NomeSharedStyle__ para estilos de outros componentes que ali serão utilizados.
///
/// A implementação fica dessa forma:
///
/// _ComponentStyle<NomeStyle, NomeSharedStyle>_

class ComponentStyle<T, U> {
  final T? regular;
  final T? loading;
  final T? error;
  final T? disabled;
  final T? empty;
  final U shared;

  const ComponentStyle({
    this.regular,
    this.loading,
    this.error,
    this.disabled,
    this.empty,
    required this.shared,
  });

  T create(T Function() creator) => creator();
}
