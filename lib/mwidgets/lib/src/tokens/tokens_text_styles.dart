import 'package:flutter/material.dart';

abstract class MTextStyles {
  MTextStyles._();

  /// [fontFamily] 'RobotoFlex', [fontSize] 36, [fontWeight] 700
  static const TextStyle headline36Bold = TextStyle(
    fontFamily: 'RobotoFlex',
    fontSize: 36,
    fontWeight: FontWeight.w700,
    height: 1.2,
  );

}
