abstract class MSizes {
  MSizes._();

  /// Define tamanho para `0.0`
  static const double zero = 0.0;

  /// Define tamanho para `1.0`
  static const double s1 = 1.0;

  /// Define tamanho para `2.0`
  static const double s2 = 2.0;

  /// Define tamanho para `4.0`
  static const double s4 = 4.0;

  /// Define tamanho para `8.0`
  static const double s8 = 8.0;

  /// Define tamanho para `16.0`
  static const double s16 = 16.0;

  /// Define tamanho para `24.0`
  static const double s24 = 24.0;

  /// Define tamanho para `32.0`
  static const double s32 = 32.0;

  /// Define tamanho para `40.0`
  static const double s40 = 40.0;

  /// Define tamanho para `48.0`
  static const double s48 = 48.0;

  /// Define tamanho para `56.0`
  static const double s56 = 56.0;

  /// Define tamanho para `64.0`
  static const double s64 = 64.0;

  /// Define tamanho para `128.0`
  static const double s128 = 128.0;
}
