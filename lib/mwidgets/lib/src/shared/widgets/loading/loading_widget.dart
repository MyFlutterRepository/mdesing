import 'package:flutter/material.dart';
import 'loading_style.dart';
import 'package:shimmer/shimmer.dart';

class LoadingWidget extends StatelessWidget {
  final LoadingStyle style;
  final Size? size;

  const LoadingWidget({
    Key? key,
    required this.style,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: style.borderRadius,
      child: SizedBox(
        width: size?.width ?? style.size.width,
        height: size?.height ?? style.size.height,
        child: Shimmer.fromColors(
          baseColor: style.baseColor,
          highlightColor: style.highlightColor,
          child: Container(color: style.highlightColor),
        ),
      ),
    );
  }
}
