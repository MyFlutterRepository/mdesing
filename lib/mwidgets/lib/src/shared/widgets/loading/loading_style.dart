import 'package:flutter/material.dart';

class LoadingStyle {
  final Size size;
  final Color baseColor;
  final Color highlightColor;
  final BorderRadiusGeometry borderRadius;

  LoadingStyle({
    required this.size,
    required this.baseColor,
    required this.highlightColor,
    required this.borderRadius,
  });
}
