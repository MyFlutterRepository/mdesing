import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/core/component/component_style.dart';
import 'package:flutter/material.dart';
import 'package:mwidgets/src/{{type}}/{{name.snakeCase()}}/component/{{name.snakeCase()}}_component.dart';
import 'package:mwidgets/src/{{type}}/{{name.snakeCase()}}/component/{{name.snakeCase()}}_component_style.dart';

class {{name.pascalCase()}}Fixture {
  static get style => ComponentStyle<
          {{name.pascalCase()}}ComponentStyle,
          {{name.pascalCase()}}ComponentSharedStyle>(
        regular: {{name.pascalCase()}}ComponentStyle(),
        loading: {{name.pascalCase()}}ComponentStyle(),
        shared: {{name.pascalCase()}}ComponentSharedStyle(),
      );
  
  static render(
    Behaviour behaviour,
    String? labelSemantics,
    String? hintSemantics,
    Key key,
  ) => {{name.pascalCase()}}Component(
    componentStyle: style,
    behaviour: behaviour,
    labelSemantics: labelSemantics,
    hintSemantics: hintSemantics,
    key: key,
  ) ;
}