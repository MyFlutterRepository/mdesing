import '../../../core/component/component_style.dart';
import 'component/{{name.snakeCase()}}_component.dart';
import 'component/{{name.snakeCase()}}_component_style.dart';

abstract class {{name.pascalCase()}}Specs  extends {{name.pascalCase()}}Component {

  /// Put your comment here
  const {{name.pascalCase()}}Specs ({
    required super.behaviour,
    required super.componentStyle,
    super.labelSemantics,
    super.hintSemantics,
    super.key,
  });

  static standardStyle() => ComponentStyle<
          {{name.pascalCase()}}ComponentStyle,
          {{name.pascalCase()}}ComponentSharedStyle>(
        regular: {{name.pascalCase()}}ComponentStyle(),
        loading: {{name.pascalCase()}}ComponentStyle(),
        shared: {{name.pascalCase()}}ComponentSharedStyle(),
      );
}
