import 'package:flutter/material.dart';
import '{{name.snakeCase()}}_component_style.dart';
import '../../../../core/behaviour/behaviour_helper.dart';
import '../../../../core/component/component.dart';
import '../../../../core/behaviour/behaviour.dart';
import '../../../../core/component/component_style.dart';

class {{name.pascalCase()}}Component extends StatelessWidget with
    Component<{{name.pascalCase()}}ComponentStyle,
              {{name.pascalCase()}}ComponentSharedStyle> {
  /// Comportamento do Componente
  final Behaviour behaviour;

  /// Estilo do Componente
  final ComponentStyle<{{name.pascalCase()}}ComponentStyle,
      {{name.pascalCase()}}ComponentSharedStyle> componentStyle;


  /// Label de Acessibilidade do Componente
  final String? labelSemantics;

  /// Hint de Acessibilidade do Componente
  final String? hintSemantics;


  const {{name.pascalCase()}}Component({
    required this.behaviour,
    required this.componentStyle,
    this.labelSemantics,
    this.hintSemantics,
    super.key,
  });

  @override
  Widget whenRegular(
    {{name.pascalCase()}}ComponentStyle style,
    {{name.pascalCase()}}ComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Container(),
    );
  }

  @override
  Widget whenLoading(
    {{name.pascalCase()}}ComponentStyle style,
    {{name.pascalCase()}}ComponentSharedStyle sharedStyle,
    BuildContext context,
    Behaviour childBehaviour,
  ) {
    return Semantics(
      label: labelSemantics,
      hint: hintSemantics,
      child: Container(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return super.render(
      context: context,
      behaviour: behaviour,
      componentStyle: componentStyle,
    );
  }

  @override
  Map<Behaviour, Behaviour>? get delegate => BehaviourHelper.regularAndLoading;
}
