import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/{{type}}.dart';

class {{name.pascalCase()}}ExampleArgument {
  Behaviour behaviour;
  String? labelSemantics;
  String? hintSemantics;
  Key? key;
  {{name.pascalCase()}}ExampleArgument({
    required this.behaviour,
    this.labelSemantics,
    this.hintSemantics,
    this.key,
  });
}

abstract class {{name.pascalCase()}}Example {
  {{name.pascalCase()}}Example._();

  static Map<String, Widget> examples({
    required {{name.pascalCase()}}ExampleArgument defaultArgs,
    Map<String, {{name.pascalCase()}}ExampleArgument>? args,
  }) {
    argsBuild(String style) => _factory(defaultArgs, args, style);
    return {
      'standard': M{{name.pascalCase()}}.standard(
        behaviour: argsBuild('standard').behaviour,
        labelSemantics: argsBuild('standard').labelSemantics,
        hintSemantics: argsBuild('standard').hintSemantics,
        key: argsBuild('standard').key,
      ),
    };
  }

  static get name => 'M{{name.pascalCase()}}';

  static {{name.pascalCase()}}ExampleArgument _factory(
    {{name.pascalCase()}}ExampleArgument defaultArgs,
    Map<String, {{name.pascalCase()}}ExampleArgument>? args,
    String style,
  ) {
    if (args?.containsKey(style) ?? false) {
      return args![style]!;
    } else {
      return defaultArgs;
    }
  }
}
