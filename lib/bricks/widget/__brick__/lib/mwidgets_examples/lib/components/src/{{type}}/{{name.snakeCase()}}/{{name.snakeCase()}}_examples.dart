import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';


import '../../../component_example.dart';
import '{{name.snakeCase()}}_examples.gen.dart';


class M{{name.pascalCase()}}Examples extends ComponentExample {

  @override
  Map<String, Widget> examples({
    required Behaviour behaviour, 
    BuildContext? context,
  }) {
    return {{name.pascalCase()}}Example.examples(
      defaultArgs: {{name.pascalCase()}}ExampleArgument(
        behaviour: behaviour,
      ),
    );
  }

  @override
  String get getName => {{name.pascalCase()}}Example.name;
}
