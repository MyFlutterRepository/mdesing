import 'dart:io';

import 'package:mason/mason.dart';

void run(HookContext context) async {
  final dir = Directory('.');
  final List<FileSystemEntity> entities = await dir.list().toList();
  final entitiesName = entities.map((e) => e.uri);
  if (!entitiesName.contains(Uri.parse(r"./.is_root"))) {
    context.logger.alert(
      "Certifies that you are in root directory (contains the lib/mwidgets.dart)!! ",
    );
    exit(1);
  }
  final cachePath = Uri.parse(
    'lib/bricks/${context.vars["type"]}s_cache.txt',
  ).path;
  final cache = File(
    cachePath,
  ).readAsLinesSync();
  if (cache.any((name) => name == context.vars["name"])) {
    final abort = context.logger.confirm(
      "There is a component with this name, do you want abort?",
      defaultValue: true,
    );
    if (abort) exit(1);
  }

  context.vars["type"] = '${context.vars["type"]}s';
}
