import 'package:mason/mason.dart';

import 'class_model.dart';

class ExampleTemplate {
  Class classModel;
  String name;
  String type;

  ExampleTemplate({
    required this.classModel,
    required this.name,
    required this.type,
  });

  String toDart() => '''
import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';
import 'package:mwidgets/exports/${type}s.dart';


class $_argumentClassName$_generics {
  $_fields;
  $_argumentClassName({
    $_constructor,
  });
}

abstract class ${name.pascalCase}Example {
  ${name.pascalCase}Example._();


  static Map<String, Widget> examples$_generics({
    required $_argumentClassName$_generics defaultArgs,
    Map<String,  $_argumentClassName$_generics>? args,
  }) {
    argsBuild(String style) => _factory$_generics(defaultArgs, args, style);
    return {$_examples,};
  }

  static get name => 'M${name.pascalCase}';

  static $_argumentClassName$_generics _factory$_generics(
    $_argumentClassName$_generics defaultArgs,
    Map<String, $_argumentClassName$_generics>? args,
    String style,
  ) {
    if(args?.containsKey(style) ?? false){
      return args![style]!;
    }else {
      return defaultArgs;
    }
  }
}
''';

  String get _imports => classModel.imports
      .map((imp) {
        if (imp.contains("import 'package:flutter/material.dart'")) {
          return null;
        } else {
          return "import '$imp';";
        }
      })
      .where((element) => element != null)
      .join('\n');

  String get _argumentClassName => '${name.pascalCase}ExampleArgument';

  String get _generics =>
      classModel.generics.isEmpty ? '' : '<${classModel.generics.join(',')}>';

  String get _fields => classModel.constructrArguments
      .map(
        (param) => param.name == 'componentStyle'
            ? null
            : '\n${param.type} ${param.name}',
      )
      .where((element) => element != null)
      .join(';');

  String get _constructor => classModel.constructrArguments
      .map(
        (param) => param.name != 'componentStyle'
            ? [
                param.isRequired ? 'required ' : '',
                'this.${param.name}',
                param.defaultValue.isNotEmpty ? ' = ${param.defaultValue}' : '',
              ].join()
            : null,
      )
      .where((element) => element != null)
      .join(',');

  String get _examples => classModel.styles
      .map(
        (style) =>
            "'$style': M${name.pascalCase}.$style(${_fillArguments(style)},)",
      )
      .join(',\n\n');

  String _fillArguments(String style) => classModel.constructrArguments
      .map(
        (param) => param.name == 'componentStyle'
            ? null
            : "${param.name}: argsBuild('$style').${param.name}",
      )
      .where((element) => element != null)
      .join(',');
}
