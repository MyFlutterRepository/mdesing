import 'dart:io';

import 'package:mason/mason.dart';

void main(List<String> args) {
  ChooseOptions.chooseOne(
      values: List.generate(40, (index) => 'um_widget_$index'));
}

abstract class ChooseOptions {
  static int _index = 0;
  static String _search = "";
  static List<String> _choices = [];
  static List<String> _selected = [];

  static final _tab = [9, 67, 68, 65, 66];
  static final _enterKey = [13, 10];
  static final _returnKey = [8, 127];
  static final _spaceKey = [32];

  static List<String> chooseOne({
    required List<String> values,
  }) {
    _index = 0;
    _search = "M";
    _choices = values;
    _selected = [];

    stdin
      ..echoMode = false
      ..lineMode = false;

    var finalized = false;
    int byte = 0;
    while (!finalized) {
      _writeChoices();
      byte = stdin.readByteSync();
      if (_enterKey.contains(byte)) {
        finalized = _writeConclusion();
      } else if (_tab.contains(byte)) {
        _index++;
        if (_choices.isNotEmpty) {
          _index = _index % _choices.length;
        } else {
          _index = 0;
        }
      } else if (_spaceKey.contains(byte)) {
        //Space
        if (_choices.isNotEmpty) {
          if (_selected.contains(_choices[_index])) {
            _selected.remove(_choices[_index]);
          } else {
            _selected.add(_choices[_index]);
          }
        }
      } else {
        if (_returnKey.contains(byte)) {
          if (_search.isNotEmpty) {
            _search = _search.substring(0, _search.length - 1);
          }
        } else {
          _search = '$_search${String.fromCharCode(byte)}';
        }
        _choices = values
            .where((choice) => 'M${choice.pascalCase}'.contains(_search))
            .toList();
        _index = 0;
      }
    }

    return _selected;
  }

  static bool _writeConclusion() {
    if (_selected.isNotEmpty) {
      stdin
        ..lineMode = true
        ..echoMode = true;
      stdout
        ..write('\x1b[0G') // Restore the cursor to begin of line
        ..write('\x1b[J') // clear to end of screen
        ..write('\x1b[?25h') // show cursor
        ..write(green.wrap('->'))
        ..writeln(
          " Chooses:",
        );
      var column = 0;

      for (final selection in _selected) {
        var isNotLast = selection != _selected.last;

        stdout.write(
          styleDim.wrap('  ${lightCyan.wrap('M${selection.pascalCase}')}'),
        );
        column++;
        stdout.write('\t\t');
        if (column > 3) {
          column = 0;
          if (isNotLast) {
            stdout.write('\n');
          }
        }
      }

      stdout.write('\n');
      return true;
    } else {
      stdout
        ..write('\x1b[0G') // Restore the cursor to begin of line
        ..write('\x1b[J') // clear to end of screen
        ..write('\x1b[?25h') // show cursor
        ..write(
          yellow.wrap(
            "\nYou don't choosed nothing, are you sure? (Enter to confirm)",
          ),
        );
      var confirm = stdin.readByteSync();
      if (_enterKey.contains(confirm)) {
        return true;
      } else {
        return false;
      }
    }
  }

  static void _writeChoices() {
    stdout
      ..write('\x1b[?25l') // hide cursor
      ..write('\x1b[2J') //Erase screen
      ..write('\x1b[1;0H') //Restore the cursor
      ..write('${green.wrap('?')} What is the widget name?')
      ..writeln(' (Tab -> change, Space -> (un)select, Enter -> confirm)')
      ..write(green.wrap('? '))
      ..write(_search)
      ..write('\x1b7') // save cursor
      ..write('\n');

    stdout.writeln("Selected:");
    var selectLines = 0;
    if (_selected.isNotEmpty) {
      var column = 0;

      for (final selection in _selected) {
        var isNotLast = selection != _selected.last;

        stdout.write('  ${lightCyan.wrap('M${selection.pascalCase}')}');
        column++;
        stdout.write('\t\t');
        if (column > 3) {
          column = 0;
          if (isNotLast) {
            stdout.write('\n');
            selectLines++;
          }
        }
      }
    } else {
      stdout.write("  Empty ... ");
    }

    stdout.writeln("\nChoices:");
    if (_choices.isNotEmpty) {
      ///The number max of choices showed
      final linesOff = stdout.terminalLines - selectLines - 5;
      var maxChoiceShow = (linesOff) * 4;
      final aux = (maxChoiceShow * ((_index + 1) / maxChoiceShow).floor() - 1);
      final skip = aux < 0 ? 0 : aux;

      var lines = linesOff;
      var column = 0;
      for (final choice in _choices.skip(skip).toList()) {
        for (var i = column; i > 0; i--) {
          stdout.write('\t\t\t\t');
        }

        var isNotLast = choice != _choices.last;
        if (maxChoiceShow == 1 && isNotLast) {
          stdout.write('  More itens ...');
          break;
        }
        final checkbox = _selected.contains(choice) ? lightCyan.wrap('◉') : '◯';
        if (_index == _choices.indexOf(choice)) {
          stdout
            ..write(green.wrap('❯'))
            ..write(' $checkbox  M${choice.pascalCase}');
        } else {
          stdout.write('  $checkbox  M${choice.pascalCase}');
        }
        lines--;
        if (lines == 0) {
          column++;

          lines = linesOff;
          stdout.write('\x1b[0G'); //Move the cursor to the begin of the column
          if (linesOff > 1) {
            stdout.write('\x1b[${linesOff - 1}A');
          } //Move to the begin of line
        } else {
          stdout.write('\n');
        }
        maxChoiceShow--;
      }
    } else {
      stdout.writeln("  Empty ... ");
    }

    stdout
      ..write('\x1b[?25h') //make cursor visible
      ..write('\x1b8'); //return to same line position
  }
}
