import 'package:flutter/material.dart';
import 'package:mwidgets/core/core.dart';

abstract class ComponentExample {
  String get getName;
  Map<String, Widget> examples({
    required Behaviour behaviour,
    BuildContext? context,
  });
}
