import './src/atoms/atoms.dart';
import './src/molecules/molecules.dart';
import './src/organisms/organisms.dart';
import 'component_example.dart';

class Components {
  static List<ComponentExample> get tokens {
    return [];
  }

  static List<ComponentExample> get atoms {
    return [];
  }

  static List<ComponentExample> get molecules {
    return [];
  }

  static List<ComponentExample> get organisms {
    return [];
  }
}
